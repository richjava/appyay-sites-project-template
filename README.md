# BuiltJS Windy

# Build process
- Get components
- Get layout
- Get Tailwind config
- Get styles
- Write page files
- Write settings data:
  - collections
  - pages
- Write package.json dependencies

### TODO
- Change image "src" to "url"
- Remove "app" from Windy, add to CMS
- blog-article to blog-item
- blog-item or blog-items?
