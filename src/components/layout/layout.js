import Header from "./header-1";
import Footer from "./footer-1";

const Layout = (props) => {
  const { header1Content, footer1Content, children } = props;
  return (
    <>
      <Header content={header1Content}/>
      {children}
      <Footer content={footer1Content}/>
    </>
  );
};

export default Layout;
